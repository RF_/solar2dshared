local M = {};
local options ={ width = 256, height = 256, numFrames = 2,
    sheetContentWidth = 512,
    sheetContentHeight = 256
};
local sheet = graphics.newImageSheet( 'shared/assets/mute.png', options );
local muted = false;
local channel = nil;
local function toggle(event)
    if (event.phase == 'ended') then
        if (muted) then
            audio.setVolume(1, {channel=channel});
            M.obj.fill = {
                type='image',
                sheet=sheet,
                frame=2
            }
        else
            audio.setVolume(0, {channel=channel});
            M.obj.fill = {
                type='image',
                sheet=sheet,
                frame=1
            }
        end
        muted = not muted;
    end
end

function M:show ()
    self.obj.alpha = 1;
end

function M:hide ()
    self.obj.alpha = 0;
end

function M:init(g, c, x, y)
    channel = c;
    self.obj = display.newImage(g, sheet, 2,
    x or display.x0+0.1*display.contentWidth,
    y or display.y0+0.1*display.contentWidth);
    self.obj.width = 0.08*display.contentWidth;
    self.obj.height = 0.08*display.contentWidth;
    self.obj:addEventListener('touch', toggle )
    audio.setVolume(1, {channel=channel});
end
return M;