local M = {};
M.group = nil;
M.bg = {0,0,0,0};
M.message = "Have you played Stack Pattern? \nhttps://play.google.com/store/apps/details?id=com.pchas.game.stackpattern";
local obj = nil

local function captureScreen()
    obj.alpha = 0;
    local screenshot = "share.png";
    display.save( M.group, { filename=screenshot, baseDir=system.DocumentsDirectory  , captureOffscreenArea=false, backgroundColor=M.bg } )
    obj.alpha = 1;
    return screenshot;
end

local function shareHanler ()
    print(M.group);
    if (M.group ~= nil) then
        local screenshot = captureScreen()
        print(screenshot);
        timer.performWithDelay(200, function(params)
            native.showPopup('social', {
                message=M.message,
                image={
                    {baseDir=system.DocumentsDirectory  , filename = screenshot}
                },
            }); 
        end);
    elseif (M.message ~= nil) then
        native.showPopup('social', {
            message=M.message
        });
    end
end

function M:setConfig(config)
      self.bg = config.bg or self.bg;
      self.message = config.message or self.message;
end

local function touch(event)
    if (event.phase == 'began') then
        transition.to(obj, {time=100, xScale=1.1, yScale=1.1});
    elseif (event.phase == 'moved' and not event.target.isFocus) then
        transition.to(obj, {time=100, xScale=1, yScale=1});
    elseif (event.phase == 'ended') then
        transition.to(obj, {time=100, xScale=1, yScale=1});
        shareHanler()
        muted = not muted;
    end
end

function M:show()
    obj.alpha = 1;
end

function M:hide()
    obj.alpha = 0;
end

function M:init(parent, group, x, y)
    M.group = group;
    obj = display.newImage(parent or display.newGroup(), 'shared/assets/share.png', x or display.x-0.1*display.contentWidth, y or display.y0+0.1*display.contentWidth);
    obj.width = 0.08*display.contentWidth;
    obj.height = obj.width;
    obj:addEventListener('touch', touch )

end
return M