local M = {};

function M.init ()
    local topInset, leftInset, bottomInset, rightInset = display.getSafeAreaInsets()

    local y0 = display.screenOriginY + topInset;
    local x0 = display.screenOriginX + leftInset;
-- Create a vector rectangle sized exactly to the "safe area"
    local safeArea = display.newRect(
        x0, 
        y0, 
        display.actualContentWidth - ( leftInset + rightInset ), 
        display.actualContentHeight - ( topInset + bottomInset )
    )
    safeArea.alpha = 0.1;
    safeArea:setFillColor(1,0,0);
    safeArea:translate( safeArea.width*0.5, safeArea.height*0.5 )
    local x = safeArea.x*2;
    local y = safeArea.y*2;
    local centerX = safeArea.x;
    local centerY = safeArea.y;
    display.remove(safeArea);
    display.contentWidth = x-x0;
    display.contentHeight = y-y0;
    display.contentCenterX = centerX;
    display.contentCenterY = centerY;
    display.screenOriginX = x0;
    display.screenOriginY = y0;
    display.x0 = x0;
    display.y0 = y0;
    display.x = x;
    display.y = y;
    safeArea, topInset, leftInset, bottomInset, rightInset, x0, y0, x, y = nil, nil, nil, nil, nil, nil, nil, nil, nil;
end

return M;