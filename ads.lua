local config = require('game-config');
local M = {
    test = 10
}
local listener = nil
if (config.admob.showAds and system.getInfo("platform") == "android" and system.getInfo("environment") == "device") then
    local admob = require( "plugin.admob" )

    --- func desc
    ---@param adType string {'banner', 'rewardedVideo', 'interstitial'}
    local function load (type)
        if (admob.isLoaded(type)) then return end;
        if (not pcall(admob.load, type,{
            adUnitId=config.admob[type];
        })) then
            print('error loading ad - E = 1 - ads')
        end
    end

    local function loadRewardAd ()
        if (not pcall(admob.load, 'rewardedVideo',{
            adUnitId=config.admob['rewardedVideo']
        })) then
            print('error loading ad - E = 2 - ads')
        end
    end

    local function loadInterstitialAd ()
        if (not pcall(admob.load, 'interstitial',{
            adUnitId=config.admob['interstitial']
        })) then
            print('error loading ad - E = 3 - ads')
        end
    end

    local function loadBannerdAd ()
        if (not pcall(admob.load, 'banner',{
            adUnitId=config.admob['banner']
        })) then
            print('error loading ad - E = 4 - ads')
        end
    end

    local function adRequestListener (event)
        -- native.showAlert( "listener", event.phase)
        if(event.phase == "init") then
            load('rewardedVideo');
            load('interstitial');
            -- loadBannerdAd();
        elseif (event.phase == "failed") then
            timer.performWithDelay(10000, function()
                load(event.type)   
            end);
        elseif (event.phase == "reward") then
            if (listener) then
                listener({done=true})
            end
        elseif (event.phase == "closed") then
            if (listener and event.type=='interstitial') then
                listener({done=false})
            end
            load(event.type);
        end
    end

    --- func desc
    ---@param l function
    ---@param adType string {'rewardedVideo', 'interstitial'}
    function M.showAd (l, adType )
        listener = l
        if (admob.isLoaded(adType)) then
            if (not pcall(admob.show, adType)) then
                print('error showing ad - E = 5 - ads')
            end
        elseif (listener) then
            listener({done=false})
            -- timer.performWithDelay(1000,showAd,1);
        end
    end

    function M:showBanner ()
        if (admob.isLoaded('banner')) then
            admob.show('banner');
        end
    end

    --- func desc
    ---@param adType string {'rewardedVideo', 'interstitial', 'banner'}
    function M:isLoaded (adType)
        return admob.isLoaded(adType);
    end


    --- func desc
    --- only for banners
    function M.hide ()
        admob.hide();
        loadBannerdAd();
    end

    admob.init(adRequestListener,{
        testMode=config.admob.adsTestMode
    });
else
    function M.showAd (l, adType)
        listener = l
        if (listener) then
            listener({done=true})
        end
    end

    function M:showBanner ()
        print("SHOWING BANNER AD");
    end

    function M.hide ()
        print("HIDING BANNER AD");
    end

    function M:isLoaded ()
        return true;
    end
end
return M